# ⚙ HTML, CSS and SASS Boilerplate 
This project is used as a boilerplate for tasks in the "HTML, CSS and SASS" course in boom.dev

🤯💥💣

## JavaScript Task
* Await of the Planets.
## Objective
* Checkout the dev branch.
* Request **all planets from all pages** https://swapi.boom.dev/api/planets and **visualize the as boxes** on the page
* When implemented merge the dev branch to master.
## Requirements
* Start the project with **npm run start**.
* First request **must not have** a **page** query parameter.
* There must be a property named **_loading** defined in Application.js which is a **<progress>** element.
* There must be method named **_load** defined in Application.js.
* There must be method named **_create** defined in Application.js.
* There must be method named **_startLoading** defined in Application.js.
* There must be method named **_stopLoading** defined in Application.js.
* Fetch must be used inside of the **_load** method.
* Await must be used inside of the **_load** method.
* The **_load** method must be async.
* All planets must be visualized as boxes on the page.
* The loading bar must be visible while makong the requests.
* The loading bar must hide when the requests are completed.
##Gotchas
* Use the already defined **progressbar** on the page
* Move the rendering of the boxes inside of the **_create** method
