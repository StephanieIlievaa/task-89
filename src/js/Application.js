import EventEmitter from "eventemitter3";
import image from "../images/planet.svg";
import 'regenerator-runtime/runtime';

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  //add to the constructor - select the element <progress>
  constructor() {
    super();
    this._startLoading();
    this._loading = document.querySelector('progress');
    this.emit(Application.events.READY);
  }
  //https://swapi.boom.dev/api/planets
  //async _load -> await, fetch
  async _load() {
    let apiUrl = "https://swapi.boom.dev/api/planets"
    let planets = [];
    let res = await fetch(apiUrl);

    let { next, results } = await res.json();

    planets = [...results];
    while (next !== null) {
      let res = await fetch(next);
      let result = await res.json();
      planets = [...planets, ...result.results];
      next = result.next;
    }

    planets.forEach(({ name, terrain, population }) => this._create(name, terrain, population));
    this._stopLoading();
  }
  //rendering of the boxes inside of the _create method
  //add the parameters 
  _create(name, terrain, population) {
    const box = document.createElement("div");
    box.classList.add("box");
    box.innerHTML = this._render({
      name: name,
      terrain: terrain,
      population: population,
    });
    document.body.querySelector(".main").appendChild(box);
  }
  async _startLoading() {
    await this._load();
  }
  _stopLoading() {
    //loading bar must be hide when the requests are completed
    this._loading.style.visibility = 'hidden';
  }

  //temlate to visualize all the planets
  _render({ name, terrain, population }) {
    return `
<article class="media">
  <div class="media-left">
    <figure class="image is-64x64">
      <img src="${image}" alt="planet">
    </figure>
  </div>
  <div class="media-content">
    <div class="content">
    <h4>${name}</h4>
      <p>
        <span class="tag">${terrain}</span> <span class="tag">${population}</span>
        <br>
      </p>
    </div>
  </div>
</article>
    `;
  }
}
